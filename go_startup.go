// Copyright 2020 cpke
//
// build +windows
package go_startup

import (
	"errors"
	"fmt"

	"golang.org/x/sys/windows/registry"
)

type StartupType string

var ErrInvalidStartup = errors.New("unrecognized startup type")

const (
	// StartupUser will only apply to the current user, whereas StartupMachine will have the application autostart on
	// all users on the machine
	StartupUser    StartupType = "CURRENT_USER"
	StartupMachine StartupType = "LOCAL_MACHINE"

	runPath     = `SOFTWARE\Microsoft\Windows\CurrentVersion\Run`
	runOncePath = `SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce`

	// https://docs.microsoft.com/en-us/windows/win32/setupapi/run-and-runonce-registry-keys
	SafeModePrefix      = "*"
	DeferDeletionPrefix = "!"
)

type AppConfig struct {
	Name        string
	Executable  string
	StartupType StartupType
	RunOnce     bool

	// RunOnce specific options
	SafeMode      bool
	DeferDeletion bool
}

type App struct {
	*AppConfig
	rootRegKey registry.Key
}

// NewApp creates a startup application
func NewApp(config *AppConfig) (*App, error) {
	rootRegKey, err := startupTypeToRegKey(config.StartupType)

	if err != nil {
		return nil, err
	}

	return &App{
		AppConfig:  config,
		rootRegKey: rootRegKey,
	}, nil
}

// IsEnabled returns whether or not the startup application is enabled
//
// It queries the registry, returning false if the key does not exist, or true otherwise.
// Any errors encountered that are not ErrNotExist will be returned
func (app *App) IsEnabled() (bool, error) {
	k, err := registry.OpenKey(app.rootRegKey, runPath, registry.QUERY_VALUE)
	if err != nil {
		return false, err
	}

	defer k.Close()

	if _, _, err = k.GetValue(app.Name, nil); err != nil {
		if err == registry.ErrNotExist {
			return false, nil
		}

		return false, err
	}

	return true, err
}

// Enable attempts to enable a startup application
func (app *App) Enable() error {
	var (
		keyPath  string
		execPath string = app.Executable
	)

	if app.RunOnce {
		keyPath = runOncePath

		if app.SafeMode && app.DeferDeletion {
			return fmt.Errorf("cannot specify multiple prefixes")
		} else if app.SafeMode {
			execPath = SafeModePrefix + execPath
		} else if app.DeferDeletion {
			execPath = DeferDeletionPrefix + execPath
		}
	} else {
		keyPath = runPath
	}

	k, err := registry.OpenKey(app.rootRegKey, keyPath, registry.QUERY_VALUE|registry.SET_VALUE)
	if err != nil {
		return err
	}

	defer k.Close()

	return k.SetStringValue(app.Name, execPath)
}

// Disable attempts to disable a startup application
func (app *App) Disable() error {
	k, err := registry.OpenKey(app.rootRegKey, runPath, registry.QUERY_VALUE|registry.SET_VALUE)
	if err != nil {
		return err
	}

	return k.DeleteValue(app.Name)
}

// startupTypeToRegKey converts a StartupType instance to its corresponding registry key
func startupTypeToRegKey(startupType StartupType) (registry.Key, error) {
	var (
		rootKey registry.Key = 0
		err     error
	)

	switch startupType {
	case StartupUser:
		{
			rootKey = registry.CURRENT_USER
		}
	case StartupMachine:
		{
			rootKey = registry.LOCAL_MACHINE
		}
	default:
		{
			err = ErrInvalidStartup
		}
	}

	return rootKey, err
}
