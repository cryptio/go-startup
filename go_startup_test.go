// Copyright 2020 cpke
package go_startup

import (
	"testing"

	"golang.org/x/sys/windows/registry"
)

func TestApp(t *testing.T) {
	config := &AppConfig{
		Name:        "App Test",
		StartupType: StartupUser,
	}

	app, err := NewApp(config)
	if err != nil {
		t.Errorf("failed creating app %+v: %s", app, err)
	}

	isEnabled, err := app.IsEnabled()
	if err != nil {
		t.Errorf("checking if app %+v is enabled: %s", app, err)
	}

	if isEnabled {
		t.Errorf("wrong IsEnabled() value for app %+v, expected: %t, got: %t", app, false, isEnabled)
	}

	if err = app.Enable(); err != nil {
		t.Errorf("enabling app %+v: %s", app, err)
	}

	isEnabled, err = app.IsEnabled()
	if err != nil {
		t.Errorf("checking if app %+v is enabled: %s", app, err)
	}

	if !isEnabled {
		t.Errorf("wrong IsEnabled() value for app %+v, expected: %t, got: %t", app, true, isEnabled)
	}

	if err = app.Disable(); err != nil {
		t.Errorf("disabling app %+v: %s", app, err)
	}

	isEnabled, err = app.IsEnabled()
	if err != nil {
		t.Errorf("checking if app %+v is enabled: %s", app, err)
	}

	if isEnabled {
		t.Errorf("wrong IsEnabled() value for app %+v, expected: %t, got: %t", app, false, isEnabled)
	}
}

func TestStartupTypeToRegKey(t *testing.T) {
	tests := []struct {
		startupType StartupType
		key         registry.Key
		err         error
	}{
		{
			startupType: StartupUser,
			key:         registry.CURRENT_USER,
			err:         nil,
		},
		{
			startupType: StartupMachine,
			key:         registry.LOCAL_MACHINE,
			err:         nil,
		},
		{
			startupType: "nonexistent value",
			key:         0,
			err:         ErrInvalidStartup,
		},
	}

	for _, test := range tests {
		k, err := startupTypeToRegKey(test.startupType)
		if err != test.err {
			t.Errorf("unexpected error, expected: %s, got: %s", test.err, err)
		}

		if k != test.key {
			t.Errorf("wrong key received, expected: %+v, got: %+v", test.key, k)
		}
	}
}
