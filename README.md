# go-startup

Go library for creating startup applications on Windows that execute when the system gets started.

## How it works
go-startup creates a key under `SOFTWARE\Microsoft\Windows\CurrentVersion\Run` for your application which permits it to automatically run when the system starts.

## Portability
As of right now, go-startup only supports Windows, since the library stemmed from the need to programmatically create startup programs that could optionally run in the foreground, either for all users or just the current user.

If you require a cross-platform solution for programs that can start automatically and need to run in the background with potentially elevated privileges, please examine my [service](gitlab.com/cryptio/service) repository, which is a fork of [kardianos's project](https://github.com/kardianos/service).